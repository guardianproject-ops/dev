#!/bin/bash
set -eo pipefail

if [[ $(git status --porcelain) ]]; then
  git checkout -b regenerate-readme
  git add .
  git commit -m "Regenerate README.md"
  git push -u origin regenerate-readme
  lab mr create --remove-source-branch --squash --label bot --assignee abelxluck -m "Regenerate README.md"
fi
